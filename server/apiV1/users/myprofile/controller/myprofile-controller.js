'use strict';

/**
 * Require our modules
 */
const logger = require('../../../../utils/logger');
const {
  Jokes,
  Roles,
  Branches,
  Users,
  UserFavouriteJokes,
} = require('../../../../../database/models');

const profileController = {
  getUser: id =>
    // Find new user object with new joke
    Users.findOne({
      // Include models
      include: [
        { model: Roles, attributes: ['id', 'name', 'isAdmin'] },
        { model: Branches, attributes: ['id', 'name', 'city'] },
        { model: Jokes, attributes: ['id', 'refId', 'joke'] },
      ],
      where: {
        id,
      },
    }),
  getUserFavourites: (user) => {},
  removeJokeFromFavourites: (user, id) =>
    new Promise((resolve, reject) => {
      Jokes.findOne({
        where: {
          refId: id,
        },
      })
        .then((jokeObj) => {
          UserFavouriteJokes.findOne({
            where: {
              JokeId: jokeObj.id,
            },
          }).then((favObj) => {
            favObj
              .destroy()
              .then(() => {
                logger.info(`Removed joke with id ${id} from favourites of user ${user.email}`);

                profileController.getUser(user.dataValues.id).then((userObj) => {
                  resolve(userObj);
                });
              })
              .catch((err) => {
                logger.error(err);
                reject(new Error('Failed to remove favourite'));
              });
          });
        })
        .catch((err) => {
          logger.error(err);

          reject(new Error('Failed to find favourite'));
        });
    }),
  // Adds new joke to user's favourites
  addJokeToFavourites: (user, jokeToAdd) =>
    new Promise((resolve, reject) => {
      // Find or create joke to add
      Jokes.findCreateFind({
        where: {
          refId: jokeToAdd.id,
          joke: jokeToAdd.joke,
        },
      })
        .then(jokeObj =>
          // Build new relation with joke and user
          UserFavouriteJokes.build({
            JokeId: jokeObj[0].dataValues.id,
            UserId: user.dataValues.id,
          })
            .save()
            .then((jokeRelation) => {
              logger.info(`Added joke with id ${jokeObj[0].dataValues.id} as favourite to user ${user.email}`);

              profileController.getUser(user.dataValues.id).then((userObj) => {
                resolve(userObj, jokeRelation);
              });
            })
            .catch((userErr) => {
              logger.error(`Error adding joke with id ${jokeToAdd.id} as favourite to user ${user.email}`);
              logger.error(userErr);
              reject(userErr);
            }))
        .catch((jokeErr) => {
          logger.error('Cannot find or insert joke');
          reject(jokeErr);
        });
    }),
};

module.exports = profileController;
