'use strict';

/**
 * Require our Modules
 */
const myprofile = require('express').Router();
const logger = require('../../../utils/logger');
const passport = require('passport');
const passportController = require('../../../middleware/passport/controller/passport-controller');
const controller = require('./controller/myprofile-controller');

myprofile.get('/', (req, res) => {
  // Get the loggedin User
  passportController.getLoggedInUserObject(req.headers.authorization).then(
    (userObject) => {
      // Return the User Object
      res.status(200).json({
        user: userObject,
      });
    },
    (err) => {
      const errorMessage = 'User not found!';
      logger.warn(errorMessage);

      // Return the Error
      res.status(400).json({
        err: errorMessage,
      });
    }
  );
});

myprofile.get('/favourites', (req, res) => {});

myprofile.delete('/favourites', (req, res) => {
  if (req.query.id) {
    passportController
      .getLoggedInUserObject(req.headers.authorization)
      .then((userObject) => {
        controller
          .removeJokeFromFavourites(userObject, req.query.id)
          .then((newUserObject, joke) => {
            logger.info(`Removed favourite with id ${joke} of user ${newUserObject.email}`);

            res.json({
              status: 'success',
              user: userObject,
            });
          })
          .catch((err) => {
            const errorMessage = `Failed to remove favourite joke for ${userObject.email}`;
            logger.error(errorMessage);
            logger.error(err);

            // Return the Error
            res.status(400).json({
              status: 'error',
              message: errorMessage,
            });
          });
      })
      .catch((err) => {
        const errorMessage = 'Invalid login provided';
        logger.warn(errorMessage);
        logger.warn(err);

        res.status(403).json({
          status: 'error',
          message: errorMessage,
        });
      });
  } else {
    logger.warn('Received invalid joke unfavourite request.');

    res.status(400).json({
      status: 'error',
      message: 'Please provide a joke object to remove from favourites',
    });
  }
});

myprofile.post('/favourites', (req, res) => {
  if (req.body.joke) {
    passportController
      .getLoggedInUserObject(req.headers.authorization)
      .then((userObject) => {
        controller
          .addJokeToFavourites(userObject, req.body.joke)
          .then((newUserObject, joke) => {
            logger.info(`Added new favourite with id ${joke} to user ${newUserObject.email}`);
            res.json({
              status: 'success',
              user: userObject,
            });
          })
          .catch((err) => {
            const errorMessage = `Failed to add new favourite joke for ${userObject.email}`;
            logger.error(errorMessage);
            logger.error(err);

            // Return the Error
            res.status(400).json({
              status: 'error',
              message: errorMessage,
            });
          });
      })
      .catch((err) => {
        const errorMessage = 'Invalid login provided';
        logger.warn(errorMessage);
        logger.warn(err);

        res.status(403).json({
          status: 'error',
          message: errorMessage,
        });
      });
  } else {
    logger.warn('Received invalid joke favourite request.');

    res.status(400).json({
      status: 'error',
      message: 'Please provide a joke object to add as favourite',
    });
  }
});

// Export the Myprofile
module.exports = myprofile;
