'use strict';

/**
 * Require our Modules
 */
const jokes = require('express').Router();
const logger = require('../../utils/logger');

const jokesController = require('./controller/jokes-controller');

jokes.get('/:amount', (req, res) => {
  const amount = req.params.amount || 1;

  jokesController
    .getJokes(amount)
    .then((reply) => {
      logger.info('Fetched jokes');
      res.status(200).json(reply.data.value);
    })
    .catch((err) => {
      logger.error('Error fetching jokes');
      logger.error(`Error: ${err}`);
      res.status(500).json({
        error: 'Error fetching jokes',
      });
    });
});

module.exports = jokes;
