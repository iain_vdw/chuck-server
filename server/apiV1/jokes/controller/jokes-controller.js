'use strict';

/**
 * Jokes controller
 */
const axios = require('axios');

const JOKES_ENDPOINT = 'https://api.icndb.com/jokes/random/';
const logger = require('../../../utils/logger');

const controller = {
  getJokes(amount) {
    logger.info(`Fetching ${amount} jokes`);

    return axios.get(JOKES_ENDPOINT + amount);
  },
};

module.exports = controller;
