'use strict';

/**
 * Require our modules
 */
const config = require('../../../../config/config');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const models = require('../../../../database/models');

// Define our models
const {
  Users, Roles, Branches, Jokes,
} = models;

const self = {
  // Get the Decoded Token
  getDecodedToken: (token) => {
    // Check if token is given
    if (token) {
      // Try to decode the JWT-Token
      try {
        const jwtToken = jwt.verify(token.replace('Bearer ', ''), config.jwt.secret);
        return jwtToken;
      } catch (err) {
        return false;
      }
    } else {
      return false;
    }
  },
  // Get a User Object from the Database
  getUserFromDB: guid =>
    new Promise((resolve, reject) => {
      Users.findOne({
        attributes: ['id', 'firstname', 'lastname', 'email', 'username', 'status'],
        include: [
          { model: Roles, attributes: ['id', 'name', 'isAdmin'] },
          { model: Branches, attributes: ['id', 'name', 'city'] },
          { model: Jokes, attributes: ['id', 'refId', 'joke'] },
        ],
        where: {
          guid,
        },
      }).then(
        (userObject) => {
          // Check if we have a User Object
          if (userObject) {
            resolve(userObject);
          } else {
            // No User Object found
            reject(new Error('No user found'));
          }
        },
        (err) => {
          // Could not Query
          reject(new Error('Could not query database'));
        }
      );
    }),
  // Get the loggedin User
  getLoggedInUserObject: token =>
    new Promise((resolve, reject) => {
      // Extract & Validate the Token
      const jwtToken = self.getDecodedToken(token);

      // Check if the Token is valid
      if (jwtToken) {
        // Get the User Object
        return self.getUserFromDB(jwtToken.guid).then(
          (userObject) => {
            resolve(userObject);
          },
          (err) => {
            reject(err);
          }
        );
      }

      reject(new Error('Provided login token is invalid.'));
    }),
};

module.exports = self;
