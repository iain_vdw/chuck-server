'use strict';

module.exports = (sequelize, DataTypes) => {
  const UserFavouriteJokes = sequelize.define(
    'UserFavouriteJokes',
    {
      JokeId: DataTypes.INTEGER,
      UserId: DataTypes.INTEGER,
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE,
    },
    {}
  );

  // Return the model
  return UserFavouriteJokes;
};
