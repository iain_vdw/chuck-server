'use strict';

module.exports = (sequelize, DataTypes) => {
  const Jokes = sequelize.define(
    'Jokes',
    {
      joke: DataTypes.STRING,
      refId: DataTypes.INTEGER,
    },
    {}
  );

  /**
   * @description: Define Associations
   * @param {*} models
   */
  Jokes.associate = function associateJokes(models) {
    Jokes.belongsToMany(models.Users, { through: 'UserFavouriteJokes' });
  };

  return Jokes;
};
