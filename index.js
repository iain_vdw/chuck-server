/**
 * Require our modules
 */
const https = require('https');
const config = require('./config/config');
const app = require('./server/server');
const logger = require('./server/utils/logger');
const fs = require('fs');
const initTestData = require('./database/testdata/testdata');

/**
 * For Development purposes, insert an admin user
 */
if (config.env === 'dev') {
  initTestData();
}

/**
 * HTTP Server
 * Start the HTTP-Server if it is enabled in the config
 */
if (config.server.enableHTTP) {
  app.listen(config.server.http.port, () => {
    logger.info('HTTP Server running.');
  });
}

/**
 * HTTPS Server
 * Start the HTTPS-Server if it is enabled in the config
 */
if (config.server.enableHTTPS) {
  // Define the SSL Options
  const sslOptions = {
    cert: fs.readFileSync(config.server.https.fullchainPath),
    key: fs.readFileSync(config.server.https.privatekeyPath),
  };
  https.createServer(sslOptions, app).listen(config.server.https.port, () => {
    logger.info('HTTPS Server running.');
  });
}

module.exports = app;
